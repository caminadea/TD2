

void minmax(int *t, int n, int *pmin,int *pmax){
     *pmin=t[0];
     *pmax=t[0];
     for(int i=0;i<n;i++){
         if (*pmin>t[i]){
            *pmin=t[i];
         }
         if( *pmax<t[i]){
            *pmax=t[i];
         }
     }
}

int maximum(int *t, int n){
    int max;
    max=*t;
    for(int i=0;i<n;i++){
        if (max<t[i]){
            max=t[i];
        }
    }
    return max;
}

int minimum(int *t, int n){
            int min;
            min=*t;
            for(int i=0;i<n;i++){
                if (min>t[i]){
                    min=t[i];
                }
            }
            return min;
        }

int* copie (int *tab, int n){
    int *tab2=malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        tab2[i] = tab[i];
    }
    return tab2;
}

int* unsurdeux(int *tab, int n){
    int *tab1surdeux=malloc(sizeof(int) * n/2);
    for (int i=0;i<n/2; i+=2){
        tab1surdeux[i/2]=tab[i];
    }
    return tab1surdeux;
}



int main(){
    int tab[10];
    int *pmin;
    int *pmax;
    tab[0]=0;
    tab[1]=-2;
    tab[2]=1234;
    tab[3]=829;
    tab[4]=8394;
    tab[5]=293;
    tab[6]=2;
    tab[7]=1;
    tab[8]=23;
    tab[9]=0;
    printf("%d \n", maximum(tab,10));
    printf("%d \n", minimum(tab,10));
    minmax(tab,4,&pmin,&pmax);
    printf("le min est : %d  le max est : %d\n",pmin,pmax);
}